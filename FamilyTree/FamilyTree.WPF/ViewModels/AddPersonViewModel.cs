﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using System.Windows.Threading;
using AutoMapper;
using Caliburn.Micro;
using FamilyTree.Models.Concretes;
using FamilyTree.Models.Enums;
using FamilyTree.Models.Interfaces;
using FamilyTree.Services.DataAccessLayer.db4oRepositories;
using FamilyTree.Services.DataAccessLayer.Interfaces;
using MaterialDesignThemes.Wpf;

namespace FamilyTree.WPF.ViewModels
{
    public class AddPersonViewModel : Screen
    {
        private readonly IPersonRepository _repo;

        public AddPersonViewModel()
        {
            _repo = new db4oPersonRepository("FamilyTree");
        }

        public AddPersonViewModel(IPerson persontoedit) : this()
        {
            IsEdit = true;
            OldFullName = persontoedit.FullName;
            OldMotherName = persontoedit.Mother?.FullName;
            OldFatherName = persontoedit.Father?.FullName;
            FirstName = persontoedit.FirstName;
            LastName = persontoedit.LastName;
            Father = persontoedit.Father;
            Mother = persontoedit.Mother;
            DeathDate = persontoedit.DeathDate;
            BirthDate = persontoedit.BirthDate;
            FatherName = Father?.FullName;
            MotherName = Mother?.FullName;
            IsDead = DeathDate != null;
            IsNewborn = BirthDate == null;
            IsFatherKnown = Father != null;
            IsMotherKnown = Mother != null;
            IsMen = persontoedit.Gender == eGender.Men;
            IsGenderEnabled = false;
            IsNewbornEnabled = false;
            IsNewborn = false;
        }


        private readonly Dictionary<bool, Visibility> _isVisible = new Dictionary<bool, Visibility>
        {
            {false, Visibility.Hidden},
            {true, Visibility.Visible}
        };

        private readonly Dictionary<string, eGender> _genderConverter = new Dictionary<string, eGender>
        {
            {"Men", eGender.Men},
            {"Women", eGender.Women}
        };

        private IEnumerable<IPerson> FindAncestors(IPerson person) =>
            new HashSet<IPerson>()
                .Concat(person.Father != null
                    ? new HashSet<IPerson> { person.Father }.Concat(FindAncestors(person.Father))
                    : new HashSet<IPerson>())
                .Concat(person.Mother != null
                    ? new HashSet<IPerson> { person.Mother }.Concat(FindAncestors(person.Mother))
                    : new HashSet<IPerson>());

        public IEnumerable<string> WomensList => string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName) ?
            _repo.GetAll(x => x is Women).Select(x => x.FullName).Except(new List<string> { FirstName + LastName }):
            _repo.GetAll(x => x is Women).Select(x => x.FullName);
        public IEnumerable<string> MensList => string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName) ?
            _repo.GetAll(x => x is Men).Select(x => x.FullName).Except(new List<string> { FirstName + LastName }) :
            _repo.GetAll(x => x is Men).Select(x => x.FullName);
        private IEnumerable<IPerson> ChildrenList => _repo.GetPersonByName(OldFullName)?.Childrens?.ToList();

        private string OldFullName { get;}
        private string OldFatherName { get; }
        private string OldMotherName { get; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string GenderString => IsMen ? "Men" : "Women";
        public string MotherName { get; set; }
        public string FatherName { get; set; }

        public bool IsEdit { get; set; }
        public bool IsDead { get; set; }
        public bool IsNewborn { get; set; }
        public bool IsMotherKnown { get; set; }
        public bool IsFatherKnown { get; set; }
        public bool IsMen { get; set; }
        public bool IsGenderEnabled { get; set; } = true;
        public bool IsNewbornEnabled { get; set; } = true;

        public Men Father { get; set; }
        public Women Mother { get; set; }

        public DateTime? DeathDate { get; set; }
        public DateTime? BirthDate { get; set; }

        public eGender Gender => _genderConverter.FirstOrDefault(x => x.Key == GenderString).Value;
        public Visibility BirthDateVisibility => _isVisible.FirstOrDefault(x => x.Key == !IsNewborn).Value;
        public Visibility DeathDateVisibility => _isVisible.FirstOrDefault(x => x.Key == IsDead).Value;
        public Visibility IsMotherKnownVisibility => _isVisible.FirstOrDefault(x => x.Key == IsMotherKnown).Value;
        public Visibility IsFatherKnownVisibility => _isVisible.FirstOrDefault(x => x.Key == IsFatherKnown).Value;


        private string PersonValidator(IPerson person)
        {
            if (person.Father != null && person.Mother != null)
            {
                var fatherAncestors = FindAncestors(person.Father);
                var motherAncestors = FindAncestors(person.Mother);
                var ancestors = fatherAncestors.Intersect(motherAncestors);
                if (ancestors.Any())
                {
                    return "Incestuous relationship detected";
                }
            }
            if (string.IsNullOrEmpty(person.FullName))
            {
                return "Person must have a name";
            }

            if (_repo.GetAll(x => x.FullName == person.FullName).Any() && IsEdit==false)
            {
                return "Person with this name is already in the database";
            }

            if (BirthDate > DeathDate)
            {
                return "Person can't be dead when is not even born";
            }

            if (IsFatherKnown)
            {
                var fatherValidation = FatherCheck(person,false);
                if (fatherValidation != string.Empty)
                {
                    return fatherValidation;
                }
            }

            if (IsMotherKnown)
            {
                var motherValidation = MotherCheck(person,false);
                if (motherValidation != string.Empty)
                {
                    return motherValidation;
                }
            }

            if (IsFatherKnown || IsMotherKnown)
            {
                return CicleCheck(person, new HashSet<IPerson>()) ? "Prohibited cycle detected" : string.Empty;
            }

            return string.Empty;
        }

        private string ChildrenCheck()
        {
            foreach (var child in ChildrenList)
            {
                if (IsMen)
                {
                    var fatherValidationResult = FatherCheck(child, true);
                    if (fatherValidationResult != string.Empty)
                    {
                        return fatherValidationResult;
                    }
                }
                else
                {
                    var motherValidationResult = MotherCheck(child, true);
                    if (motherValidationResult != string.Empty)
                    {
                        return motherValidationResult;
                    }
                }
                //if (child.Father != null && child.Mother != null)
                //{
                //    var ancestors = FindAncestors(child.Father).Intersect(FindAncestors(child.Mother));
                //    if (ancestors.Any())
                //    {
                //        return "Incestuous relationship detected";
                //    }
                //}

            }

            return string.Empty;
        }
        private string PersonValidator(IPerson person, bool IsEdit)
        {
            

            if (_repo.GetPersonByName(OldFullName).Childrens.Any() && person.Gender != _repo.GetPersonByName(OldFullName).Gender)
            {
                return "Person gender can't be changed, because he has children";
            }

            if (_repo.GetPersonByName(OldFullName).FullName != person.FullName && _repo.GetAll(x=>x.FullName==person.FullName).Any())
            {
                return "Person with this name is already in the database";
            }

            if (IsMotherKnown)
            {
                if (person.Mother.FullName == person.FullName)
                {
                    return "You can not be your own parent";
                }
            }
            if (IsFatherKnown)
            {
                if (person.Father.FullName == person.FullName)
                {
                    return "You can not be your own parent";
                }
            }

            var childrenValidationResult = ChildrenCheck();
            if (childrenValidationResult != string.Empty)
            {
                return childrenValidationResult;
            }

            return PersonValidator(person);


        }

        private string FatherCheck(IPerson person, bool IsFather)
        {
            if (IsFather)
            {
                if (person.BirthDate?.Year - BirthDate?.Year >= 70)
                {
                    return "Father can't be older than 70 in his child birtday";
                }

                if (person.BirthDate?.Year - BirthDate?.Year <= 12)
                {
                    return "Father can't be younger than 12 in his child birtday";
                }

                if (person.BirthDate > DeathDate?.AddDays(-270))
                {
                    return "Father can't be dead on his child conception";
                }
            }
            else
            {
                if (person.BirthDate?.Year - person.Father.BirthDate?.Year >= 70)
                {
                    return "Father can't be older than 70 in his child birtday";
                }

                if (person.BirthDate?.Year - person.Father.BirthDate?.Year <= 12)
                {
                    return "Father can't be younger than 12 in his child birtday";
                }

                if (person.BirthDate > person.Father?.DeathDate?.AddDays(-270))
                {
                    return "Father can't be dead on his child conception";
                }
            }

            return string.Empty;
        }
        private string MotherCheck(IPerson person, bool IsMother)
        {
            if (IsMother)
            {
                if (person.BirthDate?.Year - BirthDate?.Year >= 60)
                {
                    return "Mother can't be older than 60 in his child birtday";
                }

                if (person.BirthDate?.Year - BirthDate?.Year <= 10)
                {
                    return "Mother can't be younger than 10 in his child birtday";
                }

                if (person.BirthDate >= DeathDate)
                {
                    return $"Mother can't be dead on {person.FirstName} {person.LastName} birthday";
                }
            }
            else
            {
                if (person.BirthDate?.Year - person.Mother.BirthDate?.Year >= 60)
                {
                    return "Mother can't be older than 60 in his child birtday";
                }

                if (person.BirthDate?.Year - person.Mother.BirthDate?.Year <= 10)
                {
                    return "Mother can't be younger than 10 in his child birtday";
                }

                if (person.BirthDate >= person.Mother.DeathDate)
                {
                    return $"Mother can't be dead on {person.FirstName} {person.LastName} birthday";
                }
            }

            return string.Empty;
        }

        public static IEnumerable<IPerson> FindHeirs(IPerson p) => p.Childrens.Any()
            ? p.Childrens?.Select(x => x?.DeathDate == null ? new[] {x} : FindHeirs(x)).Aggregate(Enumerable.Concat): new HashSet<IPerson>();



        private bool CicleCheck(IPerson person, ISet<IPerson> visited)
        {

            _repo.Activate(person, 3);

            if (visited.Contains(person))
            {
                return true;
            }

            visited.Add(person);

            if (person.Mother != null && CicleCheck(person.Mother, visited))
            {
                return true;
            }

            if (person.Father != null && CicleCheck(person.Father, visited))
            {
                return true;
            }

            return false;
        }


        public void SavePerson()
        {
            if (!IsEdit)
            {
                if (MotherName != null)
                {
                    Mother = _repo.GetPersonByName(MotherName) as Women;
                }
                if (FatherName != null)
                {
                    Father = _repo.GetPersonByName(FatherName) as Men;
                }
                if (IsMen)
                {
                    var person = Mapper.Map<AddPersonViewModel, Men>(this);
                    if (IsNewborn)
                    {
                        person.BirthDate = DateTime.Now;
                    }
                    var validateResult = PersonValidator(person);
                    if (validateResult == string.Empty)
                    {
                        _repo.AddPerson(person);
                        UpdateParents(person);
                        ShowInfoDialog($"{person.FirstName} {person.LastName} has been added");
                        Task.Run(() =>
                        {
                            Thread.Sleep(2000);
                            Dispatcher.CurrentDispatcher.Invoke(() => { TryClose(); });
                        });
                    }
                    else
                    {
                        ShowErrorDialog(validateResult);
                    }
                }
                else
                {
                    var person = Mapper.Map<AddPersonViewModel, Women>(this);
                    if (IsNewborn)
                    {
                        person.BirthDate = DateTime.Now;
                    }
                    var validateResult = PersonValidator(person);
                   
                    if (validateResult == string.Empty)
                    {
                        _repo.AddPerson(person);
                        UpdateParents(person);
                        ShowInfoDialog($"{person.FirstName} {person.LastName} has been added");
                        Task.Run(() =>
                        {
                            Thread.Sleep(2000);
                            Dispatcher.CurrentDispatcher.Invoke(() => { TryClose(); });
                        });
                    }
                    else
                    {
                        ShowErrorDialog(validateResult);
                    }
                }
            }
            else
            {
                if (MotherName != null)
                {
                    Mother = _repo.GetPersonByName(MotherName) as Women;
                }
                if (FatherName != null)
                {
                    Father = _repo.GetPersonByName(FatherName) as Men;
                }
                if (IsMen)
                {
                    var person = Mapper.Map<AddPersonViewModel, Men>(this);
                    if (IsNewborn)
                    {
                        person.BirthDate = DateTime.Now;
                    }
                    var validateResult = PersonValidator(person,true);
                    if (validateResult == string.Empty)
                    {
                        var dbperson = _repo.GetPersonByName(FirstName + LastName);
                        if (dbperson.Childrens.Any())
                        {
                            person.Childrens = dbperson.Childrens;
                        }
                        _repo.UpdatePerson(person, OldFullName);
                        _repo.UpdateParents(person, OldFatherName, OldMotherName, OldFullName);
                        ShowInfoDialog($"{person.FirstName} {person.LastName} has been updated");
                        Task.Run(() =>
                        {
                            Thread.Sleep(2000);
                            Dispatcher.CurrentDispatcher.Invoke(() => { TryClose(); });
                        });
                    }
                    else
                    {
                        ShowErrorDialog(validateResult);
                    }
                }
                else
                {
                    var person = Mapper.Map<AddPersonViewModel, Women>(this);
                    if (IsNewborn)
                    {
                        person.BirthDate = DateTime.Now;
                    }
                    var validateResult = PersonValidator(person, true);
                    if (validateResult == string.Empty)
                    {
                        var dbperson = _repo.GetPersonByName(FirstName + LastName);
                        if (dbperson.Childrens.Any())
                        {
                            person.Childrens = dbperson.Childrens;
                        }
                        _repo.UpdatePerson(person, OldFullName);
                        _repo.UpdateParents(person,OldFatherName,OldMotherName,OldFullName);
                        ShowInfoDialog($"{person.FirstName} {person.LastName} has been updated");
                        Task.Run(() =>
                        {
                            Thread.Sleep(2000);
                            Dispatcher.CurrentDispatcher.Invoke(() => { TryClose(); });
                        });
                    }
                    else
                    {
                        ShowErrorDialog(validateResult);
                    }
                }
            }

        }

        private void UpdateParents(IPerson person)
        {
            if (IsFatherKnown)
            {
                _repo.AddChildren(person, person.Father.FullName);
            }

            if (IsMotherKnown)
            {
                _repo.AddChildren(person, person.Mother.FullName);
            }
        }
        public void ShowInfoDialog(string errorText)
        {
            DialogHost.Show(new Border()
            {
                Width = 250,
                Height = 200,
                BorderBrush = Brushes.DodgerBlue,
                BorderThickness = new Thickness(2),
                Background = new SolidColorBrush(Color.FromRgb(37, 37, 37)),
                Child = new StackPanel()
                {
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Children =
                    {
                        new Viewbox()
                        {
                            Width=48,
                            Height =48,
                            Margin = new Thickness(13,0,0,10),
                            Child = new Canvas()
                            {
                                Width=76,
                                Height=76,
                                Clip= Geometry.Parse("F1 M 0,0L 76,0L 76,76L 0,76L 0,0"),
                                Children =
                                {
                                    new Path()
                                    {
                                        Width= 25,
                                        Height=58,
                                        Stretch=Stretch.Fill,
                                        Fill = Brushes.DodgerBlue,
                                        Data= Geometry.Parse("F1 M 31.6666,30.0834L 42.7499,30.0834L 42.7499,33.2501L 42.7499,52.2501L 45.9165,52.2501L 45.9165,57.0001L 31.6666,57.0001L 31.6666,52.2501L 34.8332,52.2501L 34.8332,34.8335L 31.6666,34.8335L 31.6666,30.0834 Z M 38.7917,19C 40.9778,19 42.75,20.7722 42.75,22.9583C 42.75,25.1445 40.9778,26.9167 38.7917,26.9167C 36.6055,26.9167 34.8333,25.1445 34.8333,22.9583C 34.8333,20.7722 36.6055,19 38.7917,19 Z ")
                                    }
                                },
                                Effect = new DropShadowEffect()
                                {
                                ShadowDepth = 3
                            }
                            }
                        },
                        new TextBlock()
                        {
                            Width = 200,
                            Height = 60,
                            TextWrapping = TextWrapping.WrapWithOverflow,
                            TextAlignment = TextAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Top,
                            HorizontalAlignment = HorizontalAlignment.Center,
                            Foreground = Brushes.White,
                            Text = errorText,
                            FontSize = 17,
                            Effect = new DropShadowEffect()
                            {
                                ShadowDepth = 3
                            }
                        },
                        new Button()
                        {
                            Width = 100,
                            Height = 40,
                            VerticalAlignment = VerticalAlignment.Bottom,
                            HorizontalAlignment = HorizontalAlignment.Center,
                            Content = "OK",
                            Command = DialogHost.CloseDialogCommand,
                            Margin = new Thickness(0,10,0,0),
                            Effect = new DropShadowEffect()
                            {
                                ShadowDepth = 3
                            }
                        }
                    }
                }
            }, "Error");
            SystemSounds.Beep.Play();
        }
        public void ShowErrorDialog(string errorText)
        {
            DialogHost.Show(new Border()
            {
                Width = 250,
                Height = 200,
                BorderBrush = Brushes.DodgerBlue,
                BorderThickness = new Thickness(2),
                Background = new SolidColorBrush(Color.FromRgb(37, 37, 37)),
                Child = new StackPanel()
                {
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Children =
                    {
                        new Viewbox()
                        {
                            Width=48,
                            Height =48,
                            Margin = new Thickness(13,0,0,10),
                            Child = new Canvas()
                            {
                                Width=76,
                                Height=76,
                                Clip= Geometry.Parse("F1 M 0,0L 76,0L 76,76L 0,76L 0,0"),
                                Children =
                                {
                                    new Path()
                                    {
                                        Width= 58,
                                        Height=58,
                                        Stretch=Stretch.Fill,
                                        Fill = Brushes.DodgerBlue,
                                        Data= Geometry.Parse("M13,14H11V10H13M13,18H11V16H13M1,21H23L12,2L1,21Z")
                                    }
                                },
                                Effect = new DropShadowEffect()
                                {
                                ShadowDepth = 3
                            }
                            }
                        },
                        new TextBlock()
                        {
                            Width = 200,
                            Height = 60,
                            TextWrapping = TextWrapping.WrapWithOverflow,
                            TextAlignment = TextAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Top,
                            HorizontalAlignment = HorizontalAlignment.Center,
                            Foreground = Brushes.White,
                            Text = errorText,
                            FontSize = 17,
                            Effect = new DropShadowEffect()
                            {
                                ShadowDepth = 3
                            }
                        },
                        new Button()
                        {
                            Width = 100,
                            Height = 40,
                            VerticalAlignment = VerticalAlignment.Bottom,
                            HorizontalAlignment = HorizontalAlignment.Center,
                            Content = "OK",
                            Command = DialogHost.CloseDialogCommand,
                            Margin = new Thickness(0,10,0,0),
                            Effect = new DropShadowEffect()
                            {
                                ShadowDepth = 3
                            }
                        }
                    }
                }
            }, "Error");
            SystemSounds.Beep.Play();
        }
    }
}