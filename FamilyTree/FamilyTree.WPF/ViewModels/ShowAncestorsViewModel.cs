﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Caliburn.Micro;
using FamilyTree.Models.Interfaces;
using FamilyTree.Services.DataAccessLayer.db4oRepositories;
using FamilyTree.Services.DataAccessLayer.Interfaces;

namespace FamilyTree.WPF.ViewModels
{
    public class ShowAncestorsViewModel : Screen

    {
        private readonly IPersonRepository _repo;

        public ShowAncestorsViewModel()
        {
            _repo = new db4oPersonRepository("FamilyTree");
            People = _repo.GetAll(x => true).Select(x => x.FullName);
        }


        public IEnumerable<IPerson> AncestorsList { get; set; }
        public IEnumerable<string> People { get; set; }
        public string SelectedPerson { get; set; }
        public string SelectedPerson2 { get; set; }

        private IEnumerable<IPerson> FindAncestors(IPerson person) =>
            new HashSet<IPerson>()
                .Concat(person.Father != null
                    ? new HashSet<IPerson> {person.Father}.Concat(FindAncestors(person.Father))
                    : new HashSet<IPerson>())
                .Concat(person.Mother != null
                    ? new HashSet<IPerson> {person.Mother}.Concat(FindAncestors(person.Mother))
                    : new HashSet<IPerson>());

        public void ShowAncestors()
        {
            var selectedPerson = _repo.GetPersonByName(SelectedPerson);
            var selectedPerson2 = _repo.GetPersonByName(SelectedPerson2);
            _repo.Activate(selectedPerson, 3);
            AncestorsList = FindAncestors(selectedPerson).Intersect(FindAncestors(selectedPerson2));
            var list = AncestorsList.ToList();
        }
    }
}