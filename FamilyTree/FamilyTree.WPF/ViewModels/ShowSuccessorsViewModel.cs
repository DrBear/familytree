﻿using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using FamilyTree.Models.Interfaces;
using FamilyTree.Services.DataAccessLayer.db4oRepositories;
using FamilyTree.Services.DataAccessLayer.Interfaces;

namespace FamilyTree.WPF.ViewModels
{
    public class ShowSuccessorsViewModel : Screen
    {
        private IPersonRepository _repo;

        public ShowSuccessorsViewModel()
        {
            _repo = new db4oPersonRepository("FamilyTree");
            People = _repo.GetAll(x => true).Select(x=>x.FullName);
        }


        public IEnumerable<IPerson> SuccessorsList { get; set; }
        public IEnumerable<string> People { get; set; }
        public string SelectedPerson { get; set; }

        public static IEnumerable<IPerson> FindSuccessors(IPerson p) => 
            p.Childrens.Any()?
            p.Childrens?.Select(x => x?.DeathDate == null ? new HashSet<IPerson>{ x } 
                         : FindSuccessors(x)).Aggregate(Enumerable.Concat) 
                         : new HashSet<IPerson>();

        public void ShowSuccessors()
        {
            SuccessorsList = FindSuccessors(_repo.GetPersonByName(SelectedPerson));
        }
    }
}