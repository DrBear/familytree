﻿using Caliburn.Micro;

namespace FamilyTree.WPF.ViewModels
{
    public class MainWindowViewModel : Conductor<object>
    {
        public void AddPerson()
        {
            ActivateDeactivateItem(new AddPersonViewModel());
        }

        public void ShowPeopleList()
        {
            ActivateDeactivateItem(new PeopleListViewModel());
        }
        public void ShowSuccessors()
        {
            ActivateDeactivateItem(new ShowSuccessorsViewModel());
        }
        public void ShowAncestors()
        {
            ActivateDeactivateItem(new ShowAncestorsViewModel());
        }
        public void ShowFamilyTree()
        {
            ActivateDeactivateItem(new FamilyTreeViewModel());
        }

        public void ActivateDeactivateItem(object item)
        {
            if (ActiveItem != null)
            {
                DeactivateItem(ActiveItem, false);
            }
            else
            {
                ActivateItem(item);
            }
        }

    }
}