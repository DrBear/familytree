﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using Caliburn.Micro;
using FamilyTree.Models.Interfaces;
using FamilyTree.Models.Concretes;
using FamilyTree.Services.DataAccessLayer.db4oRepositories;
using FamilyTree.Services.DataAccessLayer.Interfaces;
using FamilyTree.WPF.Utils;
using MaterialDesignThemes.Wpf;

namespace FamilyTree.WPF.ViewModels
{
    public class PeopleListViewModel : Screen
    {
        private readonly IPersonRepository _repo;
        public PeopleListViewModel()
        {
            _repo = new db4oPersonRepository("FamilyTree");
            PeopleList = _repo.GetAll(x => true).ToObservableCollection();
        }

        public BindableCollection<IPerson> PeopleList { get; set; }

        public void DeletePerson(string FirstName, string LastName, object sender)
        {
            var person = _repo.GetPersonByName(FirstName + LastName);
            var mother = _repo.GetPersonByName(person.Mother?.FullName);
            var father = _repo.GetPersonByName(person.Father?.FullName);
            var childrens = person.Childrens;

            mother?.Childrens?.Remove(person);
            father?.Childrens?.Remove(person);

            if (childrens != null && childrens.Any())
            {
                foreach (var children in childrens)
                {
                    if (person is Men)
                    {
                        children.Father = null;
                        _repo.UpdatePerson(children,children.FullName);
                    }
                    else
                    {
                        person.Mother = null;
                        _repo.UpdatePerson(children, children.FullName);
                    }
                }
            }

            if (mother!=null)
            {
                _repo.DeleteChildren(person.FullName,mother.FullName);
            }

            if (father != null)
            {
                _repo.DeleteChildren(person.FullName, father.FullName);
            }

            _repo.DeletePerson(person);
            PeopleList.Remove(person);
        }

        public void ActivateEdit(string firstname, string lastname, object sender)
        {
            var parentConductor = (Conductor<object>)(this.Parent);
            parentConductor.DeactivateItem(this,false);
            parentConductor.ActivateItem(new AddPersonViewModel(_repo.GetPersonByName(firstname+lastname)));
            this.TryClose();
        }

        public void ShowChildren(string firstname, string lastname, object sender)
        {
            var check = _repo.GetPersonByName(firstname + lastname);
            ShowInfoDialog(_repo.GetPersonByName(firstname + lastname).Childrens?.Select(x=>x?.FullName));
        }

        public void ShowInfoDialog(IEnumerable<string> Children)
        {
     
            DialogHost.Show(new Border()
            {
                Width = 250,
                Height = 200,
                BorderBrush = Brushes.DodgerBlue,
                BorderThickness = new Thickness(2),
                Background = new SolidColorBrush(Color.FromRgb(37, 37, 37)),
                Child = new StackPanel()
                {
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Children =
                    {
                        new ListBox()
                        {
                            ItemsSource = Children,
                            FontSize = 15,
                            Foreground = new SolidColorBrush(Color.FromRgb(255,255,255))
                        },
                      
                        new Button()
                        {
                            Width = 100,
                            Height = 40,
                            VerticalAlignment = VerticalAlignment.Bottom,
                            HorizontalAlignment = HorizontalAlignment.Center,
                            Content = "OK",
                            Command = DialogHost.CloseDialogCommand,
                            Margin = new Thickness(0,10,0,0),
                            Effect = new DropShadowEffect()
                            {
                                ShadowDepth = 3
                            }
                        }
                    }
                }
            }, "Error");
            SystemSounds.Beep.Play();
        }
    }
}