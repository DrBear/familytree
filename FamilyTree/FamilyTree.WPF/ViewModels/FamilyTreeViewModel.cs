﻿using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using FamilyTree.Models.Interfaces;
using FamilyTree.Services.DataAccessLayer.db4oRepositories;
using FamilyTree.Services.DataAccessLayer.Interfaces;

namespace FamilyTree.WPF.ViewModels
{
    public class FamilyTreeViewModel : Screen
    {
        private readonly IPersonRepository _repo;

        public FamilyTreeViewModel()
        {
            _repo = new db4oPersonRepository("FamilyTree");
            People = _repo.GetAll(x => true).Select(x => x.FullName);
        }


        public IEnumerable<string> People { get; set; }
        public BindableCollection<IPerson> FamilyList { get; set; } 
        public string SelectedPerson { get; set; }

        public void ShowFamilyTree()
        {
            FamilyList = new BindableCollection<IPerson> {_repo.GetPersonByName(SelectedPerson)};
        }
    }
}