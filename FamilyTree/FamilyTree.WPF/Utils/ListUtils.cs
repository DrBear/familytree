﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Caliburn.Micro;
using FamilyTree.Models.Interfaces;

namespace FamilyTree.WPF.Utils
{
    public static class ListUtils
    {
        public static BindableCollection<T> ToObservableCollection<T>(this IEnumerable<T> original)
        {
            return new BindableCollection<T>(original);
        }

    }
}