﻿using AutoMapper;
using FamilyTree.Models.Concretes;
using FamilyTree.Models.Interfaces;
using FamilyTree.WPF.ViewModels;

namespace FamilyTree.WPF.Utils
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AddPersonViewModel, Men>().ReverseMap();
            CreateMap<AddPersonViewModel, Women>().ReverseMap();
        }
    }
}