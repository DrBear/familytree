﻿using System.Windows;
using AutoMapper;
using AutoMapper.Configuration;
using Caliburn.Micro;
using FamilyTree.Models.Concretes;
using FamilyTree.WPF.Utils;
using FamilyTree.WPF.ViewModels;

namespace FamilyTree.WPF
{
    public class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainWindowViewModel>();

            Mapper.Initialize(cfg=>cfg.AddProfile(new MappingProfile()));
        }
    }
}