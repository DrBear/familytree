﻿using System;
using System.Collections.Generic;
using FamilyTree.Models.Enums;
using FamilyTree.Models.Interfaces;

namespace FamilyTree.Models.Concretes
{
    public class Women : IPerson
    {
        public string FullName => FirstName + LastName;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }
        public eGender Gender => eGender.Women;
        public Men Father { get; set; }
        public Women Mother { get; set; }
        public ICollection<IPerson> Childrens { get; set; } = new HashSet<IPerson>();
    }
}