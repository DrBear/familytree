﻿using System;
using System.Collections.Generic;
using FamilyTree.Models.Concretes;
using FamilyTree.Models.Enums;

namespace FamilyTree.Models.Interfaces
{
    public interface IPerson
    {
        string FullName { get;}
        string FirstName { get; set; }
        string LastName { get; set; }
        DateTime? BirthDate { get; set; }
        DateTime? DeathDate { get; set; }
        eGender Gender { get;}
        Men Father { get; set; }
        Women Mother { get; set; }
        ICollection<IPerson> Childrens { get; set; }

    }
}