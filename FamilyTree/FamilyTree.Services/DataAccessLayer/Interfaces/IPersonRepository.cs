﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Db4objects.Db4o;
using FamilyTree.Models.Interfaces;

namespace FamilyTree.Services.DataAccessLayer.Interfaces
{
    public interface IPersonRepository : IDisposable
    {
        void AddPerson(IPerson personToAddUpdate);
        void UpdatePerson(IPerson personToAddUpdate, string oldname);
        void DeletePerson(IPerson personToDelete);
        void DeletePersonByName(string fullname);
        IPerson GetPersonByName(string fullname);
        void Activate(IPerson person, int depth);
        void AddChildren(IPerson children, string parentName);
        void DeleteChildren(string childrenname, string parentname);
        void UpdateParents(IPerson children, string oldfathername, string oldmothername, string oldname);
        IQueryable<IPerson> GetAll(Func<IPerson, bool> predicate);
    }
}