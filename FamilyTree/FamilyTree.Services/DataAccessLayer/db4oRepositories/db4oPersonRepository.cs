﻿using System;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using Db4objects.Db4o;
using Db4objects.Db4o.Ext;
using Db4objects.Db4o.Linq;
using FamilyTree.Models.Interfaces;
using FamilyTree.Services.DataAccessLayer.Interfaces;

namespace FamilyTree.Services.DataAccessLayer.db4oRepositories
{
    public class db4oPersonRepository : IPersonRepository, IDisposable

    {
        private string _dbname { get; }
        private static IObjectContainer _db { get; set; }

        public db4oPersonRepository(string dbname)
        {
            _dbname = dbname;
            var config = Db4oEmbedded.NewConfiguration();
            config.Common.UpdateDepth = 5;

            if (_db==null)
            {
                _db = Db4oEmbedded.OpenFile(config, _dbname);
            }
        }

        public void AddPerson(IPerson personToAddUpdate)
        {
            try
            {
                _db.Ext().Store(personToAddUpdate,3);
            }
            finally
            {
               Dispose();
            }
        }
        public void UpdatePerson(IPerson personToAddUpdate, string oldfullname)
        {
            try
            {
                var dbperson = !string.IsNullOrEmpty(oldfullname) ? 
                    _db.Query<IPerson>().FirstOrDefault(x => x.FullName == oldfullname) :
                    _db.Query<IPerson>().FirstOrDefault(x => x.FullName == personToAddUpdate.FullName);

                if (dbperson?.FullName != personToAddUpdate?.FullName)
                {
                    if (!string.IsNullOrEmpty(dbperson?.LastName))
                    {
                        dbperson.LastName = personToAddUpdate?.LastName;
                    }

                    if (!string.IsNullOrEmpty(dbperson?.FirstName))
                    {
                        dbperson.FirstName = personToAddUpdate?.FirstName;
                    }
                }

                if (dbperson?.BirthDate == null || dbperson?.BirthDate != personToAddUpdate?.BirthDate)
                {
                    if (dbperson != null) dbperson.BirthDate = personToAddUpdate?.BirthDate;
                }

                if (dbperson?.DeathDate == null || dbperson?.DeathDate != personToAddUpdate?.DeathDate)
                {
                    if (dbperson != null) dbperson.DeathDate = personToAddUpdate?.DeathDate;
                }

                if (dbperson?.Father == null || dbperson?.Father != personToAddUpdate?.Father)
                {
                    if (dbperson != null) dbperson.Father = personToAddUpdate?.Father;
                }

                if (dbperson?.Mother == null || dbperson?.Mother != personToAddUpdate?.Mother)
                {
                    if (dbperson != null) dbperson.Mother = personToAddUpdate?.Mother;
                }

                if ((bool) dbperson?.Childrens.Any() && dbperson?.Childrens != null)
                {
                    if (personToAddUpdate != null && !(bool) dbperson?.Childrens.SequenceEqual(personToAddUpdate.Childrens))
                    {
                        //dbperson.Childrens = personToAddUpdate.Childrens;
                        dbperson.Childrens = _db.Query<IPerson>()
                            .FirstOrDefault(x => x.FullName == personToAddUpdate.FullName)?.Childrens;
                    }
                }
                
                _db.Ext().Store(dbperson, 3);
            }
            finally
            {
                Dispose();
            }
        }

        public void DeletePerson(IPerson personToDelete)
        {
            try
            {
                _db.Delete(_db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == personToDelete.FullName));
            }
            finally
            {
                Dispose();
            }
        }

        public void DeletePersonByName(string fullname)
        {
            try
            {
                _db.Delete(_db.AsQueryable<IPerson>().FirstOrDefault(x=>x.FullName==fullname));
            }
            finally
            {
                Dispose();
            }
        }

        public void Activate(IPerson person, int depth)
        {
            _db.Activate(person,depth);
        }

        public void AddChildren(IPerson children, string parentName)
        {
            try
            {
                var parent = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == parentName);
                parent?.Childrens.Add(children);
                _db.Store(parent);
            }
            finally
            {
                Dispose();
            }
        }
        public void DeleteChildren(string childrenname, string parentname)
        {
            try
            {
                var dbparent = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == parentname);
                var dbchildren = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == childrenname);
                dbparent?.Childrens.Remove(dbchildren);
                _db.Store(dbparent);
            }
            finally
            {
                Dispose();
            }
        }

        public void UpdateParents(IPerson children, string oldfathername, string oldmothername, string oldname)
        {
            try
            {
                var dbchildren = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == children.FullName);

                if (children.Father != null )
                {
                    if (children.Father.FullName != oldfathername && !string.IsNullOrEmpty(oldfathername))
                    {
                        var oldparent = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == oldfathername);
                        var newparent = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == children.Father.FullName);

                        oldparent?.Childrens.Remove(oldparent?.Childrens.FirstOrDefault(x=>x.FullName==oldname));
                        newparent?.Childrens.Add(dbchildren);
                        UpdatePerson(oldparent,oldfathername);
                        UpdatePerson(newparent, newparent?.FullName);
                    }
                    else
                    {
                        var parent = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == children.Father.FullName);
                        parent?.Childrens.Add(dbchildren);
                        UpdatePerson(parent, oldfathername);
                    }
                    
                }

                if (children.Mother !=null )
                {
                    if (children.Mother.FullName != oldmothername && !string.IsNullOrEmpty(oldmothername))
                    {
                        var oldparent = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == oldmothername);
                        var newparent = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == children.Mother.FullName);
                    
                        oldparent?.Childrens.Remove(oldparent?.Childrens.FirstOrDefault(x => x.FullName == oldname));
                        newparent?.Childrens.Add(dbchildren);
                        UpdatePerson(oldparent,oldmothername);
                        UpdatePerson(newparent, newparent?.FullName);
                    }
                    else
                    {
                        var parent = _db.AsQueryable<IPerson>().FirstOrDefault(x => x.FullName == children.Mother.FullName);
                        parent?.Childrens.Add(dbchildren);
                        UpdatePerson(parent, oldmothername);
                    }
                }

                
            }
            finally
            {
                Dispose();
            }
        }

        public IPerson GetPersonByName(string fullname)
        {

            return _db.Query<IPerson>().FirstOrDefault(x => x.FullName == fullname);
        }

        public IQueryable<IPerson> GetAll(Func<IPerson, bool> predicate)
        {
            return _db.Query<IPerson>().Where(predicate).AsQueryable();
        }

        public void Dispose()
        {
            _db?.Commit();
        }
    }
}